package main

import (
	"flag"
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/journald"
	"github.com/rs/zerolog/log"
	"gitlab.com/silkeh/cloudburst/influxdb"
	"gitlab.com/silkeh/cloudburst/influxdb2"
	"gitlab.com/silkeh/cloudburst/server"
)

func main() {
	var configFile string
	flag.StringVar(&configFile, "c", "config.yaml", "Configuration file")
	flag.Parse()

	// Configure console logger
	log.Logger = log.Output(zerolog.ConsoleWriter{
		TimeFormat: "2006-01-02 15:04:05", Out: os.Stderr,
	})

	// Load configuration
	log.Info().Msgf("Loading configuration from %s", configFile)
	config, err := loadConfig(configFile)
	if err != nil {
		log.Fatal().Msgf("Error loading configuration: %s", err)
	}

	// Switch logger if needed
	switch config.LogTarget {
	case "journal", "journald":
		log.Info().Msg("Logging to Systemd Journal")
		log.Logger = zerolog.New(journald.NewJournalDWriter())
	}

	// Set loglevel
	logLevel, err := zerolog.ParseLevel(config.LogLevel)
	if err != nil {
		log.Fatal().Msgf("Invalid log level: %s", err)
	}
	zerolog.SetGlobalLevel(logLevel)

	// Create the InfluxDB client
	var backend server.Backend
	if config.InfluxDB != nil {
		log.Debug().
			Str("host", config.InfluxDB.Host).
			Str("db", config.InfluxDB.Database).
			Str("user", config.InfluxDB.Username).
			Msg("Creating InfluxDB1 client")
		backend, err = influxdb.NewClient(config.InfluxDB)
	} else if config.InfluxDB2 != nil {
		log.Debug().
			Str("host", config.InfluxDB2.Host).
			Str("bucket", config.InfluxDB2.Bucket).
			Str("org", config.InfluxDB2.Org).
			Msg("Creating InfluxDB2 client")
		backend, err = influxdb2.NewClient(config.InfluxDB2)
	} else {
		log.Fatal().Msg("No InfluxDB backend configured.")
	}
	if err != nil {
		log.Fatal().Err(err).Msg("Error connecting to InfluxDB")
	}
	defer backend.Close()

	// Start the server
	srv := server.NewServer(backend)
	log.Fatal().Err(srv.ListenAndServe(config.Listeners...)).Msg("")
}
