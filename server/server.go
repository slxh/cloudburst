package server

import (
	"net/http"

	"github.com/plgd-dev/go-coap/v2/message/codes"
	"github.com/plgd-dev/go-coap/v2/mux"
	"github.com/rs/zerolog/log"
	"github.com/silkeh/senml"
)

// Server represents a Cloudburst server.
type Server struct {
	backend   Backend
	listeners []*Listener
	coapMux   *mux.Router
	httpMux   *http.ServeMux
}

// NewServer creates a server backed by a certain InfluxDB client and set of listeners.
func NewServer(backend Backend) *Server {
	server := &Server{
		backend: backend,
		coapMux: mux.NewRouter(),
		httpMux: http.NewServeMux(),
	}
	server.coapMux.DefaultHandleFunc(server.CoAPHandler)
	server.httpMux.HandleFunc("/", server.HTTPHandler)

	return server
}

// ListenAndServe starts handling requests based on the given listeners.
func (s *Server) ListenAndServe(listeners ...*Listener) error {
	ch := make(chan error)

	for _, listener := range listeners {
		l := listener
		log.Info().Str("type", l.Type).Str("address", l.Address).Msg("Starting listener")
		go func() {
			ch <- l.ListAndServe(s)
		}()
	}

	return <-ch
}

// HTTPHandler handles HTTP requests.
func (s *Server) HTTPHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	s.Handler(NewHTTPResponseWriter(w), NewHTTPRequest(r))
}

// CoAPHandler handles CoAP requests.
func (s *Server) CoAPHandler(w mux.ResponseWriter, r *mux.Message) {
	s.Handler(NewCoAPResponseWriter(w), NewCoAPRequest(r, w.Client()))
}

// Handler handles generic API requests.
func (s *Server) Handler(w ResponseWriter, req *Request) {
	if len(req.Path) == 0 || len(req.Path) > 4 || req.Path[0] != "api" || req.Path[1] != "v1" {
		errorNotFound.Write(w, req)
		return
	}

	req.log("New Request")
	err := s.apiHandler(w, req)
	if err != nil {
		err.Write(w, req)
	}
}

// apiHandler handles API requests.
func (s *Server) apiHandler(w ResponseWriter, req *Request) *Error {
	switch req.Method {
	case http.MethodGet:
		return s.getHandler(w, req)
	case http.MethodPost:
		return s.postHandler(w, req)
	default:
		return NewErrorf(codes.MethodNotAllowed, "invalid method %q", req.Method)
	}
}

// getHandler handles API GET requests.
func (s *Server) getHandler(w ResponseWriter, req *Request) *Error {
	name, err := req.Measurement()
	if err != nil {
		return NewError(codes.NotFound, err)
	}

	host := req.PathHost()
	last := req.QueryLast()
	from, to, err := req.QueryTime()
	if err != nil {
		return NewError(codes.BadRequest, err)
	}

	accept, err := findAccept(req.Accept, req.ContentType)
	if err != nil {
		return NewError(codes.UnsupportedMediaType, err)
	}

	measurements, err := s.backend.Query(name, host, from, to, last)
	if err != nil {
		log.Error().Err(err).Msg("Cannot query backend")
		return NewError(codes.InternalServerError, err)
	}

	var data []byte
	switch accept {
	case TypeApplication, TypeAny:
		accept = TypeJSON
		fallthrough
	case TypeJSON, TypeSenMLJSON:
		data, err = senml.EncodeJSON(measurements)
	case TypeCBOR, TypeSenMLCBOR:
		data, err = senml.EncodeCBOR(measurements)
	case TypeXML, TypeSenMLXML:
		data, err = senml.EncodeXML(measurements)
	default:
		return NewErrorf(codes.BadRequest, "invalid content type %q", req.Accept)
	}

	if err != nil {
		log.Error().Err(err).Msg("Cannot encode SenML")
		return NewError(codes.InternalServerError, err)
	}

	req.logResult("Request", codes.Content, data)
	err = w.SetResponse(codes.Content, accept, data)
	if err != nil {
		log.Warn().Err(err).Msg("Error sending response")
	}
	return nil
}

// postHandler handles API POST requests.
func (s *Server) postHandler(w ResponseWriter, req *Request) *Error {
	name, err := req.Measurement()
	if err != nil {
		return NewErrorf(codes.NotFound, "invalid measurement: %s", err)
	}

	data, err := req.SenML()
	if err != nil {
		return NewErrorf(codes.BadRequest, "invalid SenML: %s", err)
	}

	host, err := req.RequestHost()
	if err != nil {
		log.Warn().Str("host", host).Err(err).Msg("Could not resolve host")
	}

	err = s.backend.Write(name, host, data...)
	if err != nil {
		log.Error().Err(err).Msg("Cannot write to backend")
		return NewError(codes.InternalServerError, err)
	}

	req.logResult("Request", codes.Created, nil)
	if req.Confirmable {
		err = w.SetResponse(codes.Created, req.ContentType, nil)
		if err != nil {
			log.Warn().Err(err).Msg("Error sending response")
		}
	}
	return nil
}
