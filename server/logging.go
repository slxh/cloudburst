package server

import (
	"github.com/pion/logging"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// logFactory implements a logging.LoggerFactory.
type logFactory struct{}

// NewLogger returns a logging.LeveledLogger for this application.
func (f *logFactory) NewLogger(scope string) logging.LeveledLogger {
	return &zerologger{log.Logger.With().Str("scope", scope).Logger()}
}

// zerologger implements a logging.LeveledLogger based on Zerolog.
type zerologger struct {
	zerolog.Logger
}

func (z zerologger) Trace(msg string) {
	z.Logger.Trace().Msg(msg)
}

func (z zerologger) Tracef(format string, args ...interface{}) {
	z.Logger.Trace().Msgf(format, args...)
}

func (z zerologger) Debug(msg string) {
	z.Logger.Debug().Msg(msg)
}

func (z zerologger) Debugf(format string, args ...interface{}) {
	z.Logger.Debug().Msgf(format, args...)
}

func (z zerologger) Info(msg string) {
	z.Logger.Info().Msg(msg)
}

func (z zerologger) Infof(format string, args ...interface{}) {
	z.Logger.Info().Msgf(format, args...)
}

func (z zerologger) Warn(msg string) {
	z.Logger.Warn().Msg(msg)
}

func (z zerologger) Warnf(format string, args ...interface{}) {
	z.Logger.Warn().Msgf(format, args...)
}

func (z zerologger) Error(msg string) {
	z.Logger.Error().Msgf(msg)
}

func (z zerologger) Errorf(format string, args ...interface{}) {
	z.Logger.Error().Msgf(format, args...)
}
