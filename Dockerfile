FROM alpine:latest
COPY ["cloudburst", "/usr/local/bin/cloudburst"]
ENTRYPOINT ["/usr/local/bin/cloudburst"]
CMD ["-c", "/config.yaml"]
VOLUME ["/config.yaml"]
