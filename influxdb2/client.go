package influxdb2

import (
	"context"
	"fmt"
	"strings"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
	"github.com/silkeh/senml"
)

// ClientConfig contains InfluxDB client configuration
type ClientConfig struct {
	Host, Bucket, Org, Token string
}

// Client is an extended InfluxDB client
type Client struct {
	influxdb2.Client
	writeAPI api.WriteAPIBlocking
	queryAPI api.QueryAPI
	bucket   string
}

// NewClient returns a Client for the given configuration
func NewClient(config *ClientConfig) (*Client, error) {
	c := influxdb2.NewClient(config.Host, config.Token)
	return &Client{
		Client:   c,
		writeAPI: c.WriteAPIBlocking(config.Org, config.Bucket),
		queryAPI: c.QueryAPI(config.Org),
		bucket:   config.Bucket,
	}, nil
}

// WriteSenML writes a series of SenML measurements to InfluxDB.
func (c *Client) Write(name, host string, data ...senml.Measurement) error {
	points, err := PointsFromSenML(name, host, data)
	if err != nil || len(points) == 0 {
		return err
	}

	return c.writeAPI.WritePoint(context.Background(), points...)
}

// Query builds a query based on the given parameters and returns the result as SenML.
func (c *Client) Query(name, host string, from, to time.Time, last bool) (measurements []senml.Measurement, err error) {
	query := c.buildQuery(name, host, from, to, last)
	result, err := c.queryAPI.Query(context.Background(), query)
	if err != nil {
		return nil, err
	}
	defer result.Close()

	measurements = make([]senml.Measurement, 0, 128)
	for result.Next() {
		m, err := NewSenML(result.Record())
		if err != nil {
			return nil, err
		}
		measurements = append(measurements, m)
	}

	if result.Err() != nil {
		return nil, result.Err()
	}

	return
}

// buildQuery builds the query for the given parameters.
func (c *Client) buildQuery(name, host string, from, to time.Time, last bool) string {
	query := fmt.Sprintf(
		`from(bucket: %s)
				|> range(start: %s, stop: %s)
				|> filter(fn: (r) => r["_measurement"] == %s)`,
		formatString(c.bucket), from.Format(time.RFC3339Nano), to.Format(time.RFC3339Nano), formatString(name))

	// Add a host filter if host is set
	if host != "" {
		query += fmt.Sprintf(` |> filter(fn: (r) => r["host"] == "%s")`, formatString(host))
	}

	// Add last aggregator if needed
	if last {
		query += ` |> last()`
	}

	// Ensure that rows have all fields
	query += ` |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")`

	return query
}

// Close all connections to the InfluxDB server gracefully.
func (c *Client) Close() error {
	c.Client.Close()
	return nil
}

// formatString formats a string for a Flux query.
// See: https://docs.influxdata.com/flux/v0.65/language/lexical-elements/#string-literals
func formatString(s string) string {
	return fmt.Sprintf("%q", strings.ReplaceAll(s, `${`, `\${`))
}
