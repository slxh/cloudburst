package influxdb2

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api/query"
	"github.com/influxdata/influxdb-client-go/v2/api/write"
	"github.com/silkeh/senml"
)

// InfluxDB tag and field values.
const (
	BooleanField    = "bool"
	DataField       = "data"
	StringField     = "string"
	SumField        = "sum"
	ValueField      = "value"
	UpdateTimeField = "update_time"
	HostTag         = "host"
	NameTag         = "name"
	UnitTag         = "unit"
	TimeField       = "time"
	TimeAttribute   = "_time"
)

// PointsFromSenML creates a list of InfluxDB points from a list of SenML measurements.
func PointsFromSenML(name, host string, list []senml.Measurement) (pts []*write.Point, err error) {
	pts = make([]*write.Point, len(list))
	for i, measurement := range list {
		pts[i] = NewPoint(name, host, measurement)
	}
	return
}

// NewPoint returns an InfluxDB point for a SenML measurement.
func NewPoint(name, host string, measurement senml.Measurement) *write.Point {
	tags := map[string]string{
		HostTag: host,
		NameTag: measurement.Attrs().Name,
		UnitTag: string(measurement.Attrs().Unit),
	}

	var field string
	var value interface{}
	switch m := measurement.(type) {
	case *senml.Boolean:
		field = BooleanField
		value = m.Value
	case *senml.Data:
		field = DataField
		value = base64.StdEncoding.EncodeToString(m.Value)
	case *senml.Sum:
		field = SumField
		value = m.Value
	case *senml.String:
		field = StringField
		value = m.Value
	case *senml.Value:
		field = ValueField
		value = m.Value
	}

	fields := map[string]interface{}{
		field: value, UpdateTimeField: measurement.Attrs().UpdateTime.Seconds()}

	return influxdb2.NewPoint(name, tags, fields, measurement.Attrs().Time)
}

// NewSenML creates a new JSONPoint from an InfluxDB row
func NewSenML(row *query.FluxRecord) (m senml.Measurement, err error) {
	var attributes senml.Attributes
	var value interface{}
	var kind string

	// Parse values
	for k, v := range row.Values() {
		switch k {
		case HostTag:
			attributes.Name = v.(string) + ":" + attributes.Name
		case NameTag:
			attributes.Name += v.(string)
		case UnitTag:
			attributes.Unit = senml.Unit(v.(string))
		case TimeAttribute:
			attributes.Time, _ = v.(time.Time)
		case TimeField:
			t, _ := v.(json.Number).Int64()
			attributes.Time = time.Unix(0, t)
		case UpdateTimeField:
			switch s := v.(type) {
			case string:
				attributes.UpdateTime, _ = time.ParseDuration(s)
			case float64:
				attributes.UpdateTime = time.Duration(s * float64(time.Second))
			}
		case BooleanField, DataField, SumField, StringField, ValueField:
			if v != nil {
				kind = k
				value = v
			}
		}
	}

	if value == nil {
		return nil, fmt.Errorf("invalid row: %#v", row)
	}

	// Create SenML measurement
	switch kind {
	case BooleanField:
		m = &senml.Boolean{Attributes: attributes, Value: value.(bool)}
	case DataField:
		data, _ := base64.StdEncoding.DecodeString(value.(string))
		m = &senml.Data{Attributes: attributes, Value: data}
	case SumField:
		m = &senml.Sum{Attributes: attributes, Value: value.(float64)}
	case StringField:
		m = &senml.String{Attributes: attributes, Value: value.(string)}
	case ValueField:
		m = &senml.Value{Attributes: attributes, Value: value.(float64)}
	}

	return
}
